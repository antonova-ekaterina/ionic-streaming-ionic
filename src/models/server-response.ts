export class ServerResponse {
  data: any;
  errors: object;
  success: boolean;
}