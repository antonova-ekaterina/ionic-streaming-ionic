export interface IStream {
  id?: number;
  user?: number;
  created_at?: string;
  name: string;
  description: string;
  time_start: string;
  status: string;
  free: boolean;
  freetime: number;
  duration: number;
  price: string;
  chat_mode: boolean;
  record_url: string;
  preview: string;
  stream_name?: string;
  stream_arn?: string;
}
