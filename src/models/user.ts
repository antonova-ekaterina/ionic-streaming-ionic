export interface IUserModel {
  id: string;
  username: string;
  // name: string;
  phone_number: string;
  email: string;
  rating?: string;
  year?: string
}
