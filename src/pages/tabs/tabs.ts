import { Component } from '@angular/core'; 
 
import { ProfilePage } from '../profile/profile/profile';
import { StreamListPage } from '../stream/stream-list/stream-list';
import { UsersListPage } from '../users-list/users-list';
import { SubscribesPage } from '../subscribes/subscribes';
 
@Component({ 
  templateUrl: 'tabs.html' 
}) 
export class TabsPage { 
 
  tab1Root = StreamListPage; 
  tab2Root = ProfilePage; 
  tab3Root = UsersListPage; 
  tab4Root = SubscribesPage; 
  
 
  constructor() { 
 
  } 
} 
