import { Component } from '@angular/core';
import { UserProvider } from '../../providers/services/user.service';
import { NavParams, ToastController } from 'ionic-angular';
import { AuthServices } from '../../providers/services/auth.service';


@Component({
  selector: 'page-channel-info',
  templateUrl: 'channel-info.html',
})
export class ChannelInfoPage {
  selectUser;
  user;
  isSubscribe = false;
  userIsYou = false;
  users;
  avatar = false;

  subscribeId;
  constructor(private userProvider: UserProvider,
              public navParams: NavParams,
              private authService: AuthServices,
              private toastCtrl: ToastController) {
    this.selectUser = navParams.get('user');
    if (this.selectUser.avatar) {
      this.avatar = true;
    }
    this.user = this.authService.getUser();
  }

  ionViewWillLoad() {
    this.userProvider.getAllFavoriteChanner({"subscriber": this.user.id}).subscribe(data => {
      this.users = data
      this.users.forEach(el => {
        console.log(el)
        if (el.author == this.selectUser.id) {
          this.subscribeId = el.id;
          this.isSubscribe = true;
        }
      })
    })
    if (this.user.id == this.selectUser.id) {
      this.userIsYou = true
    } else {
      this.userIsYou = false
    }

  }

  addSubscribe() {
    console.log('add');
    this
      .userProvider
      .addFavoriteChannel(this.user.id, this.selectUser.id)
      .subscribe(data => {
        console.log('data', data)
        this.presentToast('Подписка оформлена')
      }, error => {
        this.presentToast('Произошла ошибка')
      })
  }
  removeSubscribe() {
    console.log('remove');
    // this
    //   .userProvider
    //   .removeFavoriteChannel(this.subscribeId, this.user.id, this.selectUser.id)
    //   .subscribe(data => {
    //     console.log('data', data)
    //     this.presentToast('Подписка отменена')
    //     this.isSubscribe = false;
    //   }, error => {
    //     this.presentToast('Произошла ошибка')
    //   })
  }
  presentToast(message, time = 2000) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: time,
      position: 'top'
    });  
    toast.present();
  }

}
