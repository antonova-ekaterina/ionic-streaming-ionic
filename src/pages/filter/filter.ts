import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';


@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
  type = "free";
  status = "active";
  constructor(public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
  }

  filterVal() {
    let st = this.status === "active" ? 2 : 1;
    let tp = this.type === "free" ? 1 : 2;
    let data = {status: st, type: tp};
    this.viewCtrl.dismiss(data);
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

}
