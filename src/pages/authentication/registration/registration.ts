import { Component } from '@angular/core';
import { NavController, ModalController, ToastController, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { Country } from '../../../models/form.model';
import { PhoneValidator } from '../../../validators/phone.validator';
import emailMask from 'text-mask-addons/dist/emailMask';

import { StartPage } from '../start/start';
import { AuthServices } from '../../../providers/services/auth.service';
import { TabsPage } from '../../tabs/tabs';

@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {

  validations_form: FormGroup;
  country_phone_group: FormGroup;

  emailMask = emailMask;

  countries: Array<Country>;
  constructor(public navCtrl: NavController, 
              public formBuilder: FormBuilder, 
              public modalCtrl: ModalController,
              private _authServices: AuthServices,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController) {
  }

  ionViewWillLoad() {
    this.countries = [
      new Country('RU', 'Россия'),
      new Country('UA', 'Украина')
    ];

    let country = new FormControl(this.countries[0], Validators.required);
    let phone = new FormControl('', Validators.compose([
      Validators.required,
      PhoneValidator.validCountryPhone(country)
    ]));

    this.country_phone_group = new FormGroup({
      country: country,
      phone: phone
    });

    this.validations_form = this.formBuilder.group({      
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
      ])),
      country_phone: this.country_phone_group,
      terms: new FormControl(true, Validators.pattern('true'))
    });
  }

  validation_messages = {
    'name': [
      { type: 'required', message: 'Обязательное поле.' }
    ],
    'email': [
      { type: 'required', message: 'Обязательное поле.' },
      { type: 'pattern', message: 'введите действительный адрес электронной почты.' }
    ],
    'phone': [
      { type: 'required', message: 'Обязательное для заполнения поле.' },
      { type: 'validCountryPhone', message: 'Номер телефона не соответствует выбранной стране.' }
    ],
    'terms': [
      { type: 'pattern', message: 'Для продолжения необходимо принять пользовательское соглашение.' }
    ],
  };

  onSubmit(values) {
    
    // this.presentProfileModal();
    this._authServices.signup(values.name, values.email, values.country_phone.phone).subscribe(data => {
      this.presentToast(data, 15000);
      this.presentPrompt(data, values.country_phone.phone);
    }, error => {
      console.log(error)
      if (error.description) {
        let err = error.description,
            phone = /phone_number/,
            user = /username/
        if (err.search(phone) != -1) {
          this.presentToast('Пользователь с таким номером уже зарегистрирован');
        } else if (err.search(user) != -1) {
          this.presentToast('Пользователь с таким именем уже зарегистрирован');
        }
      } else {
        this.presentToast('Произошла ошибка');
      }
    })
  }
  
  presentToast(message, time=2000) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: time,
      position: 'top'
    });  
    toast.present();
  }

  presentPrompt(code, phone) {
    let alert = this.alertCtrl.create({
      title: 'Проверочный код',
      inputs: [
        {
          name: 'code',
          placeholder: 'Введите код'
        }
      ],
      buttons: [
        {
          text: 'Закрыть',
          role: 'cancel'
        },
        {
          text: 'Отправить',
          handler: data => {
            if (code == data.code) {
              this._authServices.verification(code, phone).subscribe((data)=>{
                this.presentToast('Добро пожаловать, ' + data.username);
                this.navCtrl.push(TabsPage);
              }, error => {
                console.log(error)
              })
            } else {
              // invalid login
              this.presentToast('Проверочный код введен не верно');
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  goBack(data:object) {
    this.navCtrl.push(StartPage);
  }

}
