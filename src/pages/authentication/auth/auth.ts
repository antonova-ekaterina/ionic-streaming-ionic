import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NavController, ModalController, AlertController, ToastController } from 'ionic-angular';

import { Country } from '../../../models/form.model';
import { PhoneValidator } from '../../../validators/phone.validator';

import { StartPage } from '../start/start';
import { AuthServices } from '../../../providers/services/auth.service';
import { TabsPage } from '../../tabs/tabs';

@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {
  phone;

  validations_form: FormGroup;
  country_phone_group: FormGroup;

  countries: Array<Country>;


  constructor(public navCtrl: NavController, 
              public formBuilder: FormBuilder, 
              public modalCtrl: ModalController,
              private authServices: AuthServices,
              private alertCtrl: AlertController,
              private toastCtrl: ToastController) { }

  ionViewWillLoad() {
    this.countries = [
      new Country('RU', 'Россия'),
      new Country('UA', 'Украина')
    ];

    let country = new FormControl(this.countries[0], Validators.required);
    let phone = new FormControl('', Validators.compose([
      Validators.required,
      PhoneValidator.validCountryPhone(country)
    ]));

    this.country_phone_group = new FormGroup({
      country: country,
      phone: phone
    });

    this.validations_form = this.formBuilder.group({
      country_phone: this.country_phone_group,
      terms: new FormControl(true, Validators.pattern('true'))
    });
  }

  validation_messages = {
    'phone': [
      { type: 'required', message: 'Обязательное для заполнения поле.' },
      { type: 'validCountryPhone', message: 'Номер телефона не соответствует выбранной стране.' }
    ],
    'terms': [
      { type: 'pattern', message: 'Для продолжения необходимо принять пользовательское соглашение.' }
    ],
  };

  presentToast(message, time = 2000) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: time,
      position: 'top'
    });  
    toast.present();
  }

  presentPrompt(code) {
    let alert = this.alertCtrl.create({
      title: 'Проверочный код',
      inputs: [
        {
          name: 'code',
          placeholder: 'Введите код'
        }
      ],
      buttons: [
        {
          text: 'Закрыть',
          role: 'cancel'
        },
        {
          text: 'Отправить',
          handler: data => {
            if (code == data.code) {
              this.authServices.verification(code, this.phone).subscribe((data)=>{
                this.presentToast('Добро пожаловать, ' + data.username);
                this.navCtrl.push(TabsPage);
              }, error => {
                console.log(error)
              })
            } else {
              // invalid login
              this.presentToast('Проверочный код введен не верно');
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  onSubmit(values) {
    this.authServices.signin(values.country_phone.phone).subscribe((code) => {
      this.phone = values.country_phone.phone;
      this.presentToast(code, 15000);
      this.presentPrompt(code);
    }, error => {
      console.log(error)
      let user = /User does not exist/,
          err = error.description
      if (user.test(err)) {
        this.presentToast('Пользователь с таким номером не найден')
      }
    })
  }

  goBack(data:object) {
    this.navCtrl.push(StartPage);
  }
}
