import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthPage } from '../auth/auth';
import { RegistrationPage } from '../registration/registration';
import { GooglePlus } from '@ionic-native/google-plus';
import { AuthServices } from '../../../providers/services/auth.service';
import { TabsPage } from '../../tabs/tabs';

@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class StartPage {

  constructor(public navCtrl: NavController,
              private googlePlus: GooglePlus,
              private authServices: AuthServices) {
  }

  showAuth() {
    this.navCtrl.push(AuthPage);
  }
  showReg() {
    this.navCtrl.push(RegistrationPage);
  }
  googleAuth() {
    this.googlePlus.login({})
        .then(res => {
          console.log(res.accessToken);
          this.authServices.signupGoogle(res.accessToken).subscribe(data=>{
            this.navCtrl.push(TabsPage);
          });
        })
        .catch(msg => console.log('ERROR ', msg));
  }

}
