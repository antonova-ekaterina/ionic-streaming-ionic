import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/services/user.service';
import { AuthServices } from '../../providers/services/auth.service';

@Component({
  selector: 'page-subscribes',
  templateUrl: 'subscribes.html',
})
export class SubscribesPage {
  channels;
  infoChannels =[];
  user;

  notSubs = true;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private userProvider: UserProvider,
              private authService: AuthServices) {
    this.user = this.authService.getUser();
    this.userProvider.getAllFavoriteChanner({"subscriber": this.user.id}).subscribe(data => {
      if (!data) {
        this.notSubs = true;
        console.log('!data')
      } else {
        this.notSubs = false;
        console.log('data')
      }
      console.log(data)
      this.channels = data;
      this.channels.forEach(el => {
        this.userProvider.getSomeUser(el.author).subscribe(data => {
          this.infoChannels.push(data.user)
          console.log(this.infoChannels)
        })
      });
    })
  }

  
}
