import { Component } from '@angular/core';
import { AuthServices } from '../../providers/services/auth.service';
import { ModalController } from 'ionic-angular';
import { ChannelInfoPage } from '../channel-info/channel-info';

@Component({
  selector: 'page-users-list',
  templateUrl: 'users-list.html',
})
export class UsersListPage {
  users
  constructor(private authServices: AuthServices,
              public modalCtrl: ModalController) {
    this.authServices.getAllUser().subscribe((data) => {
      this.users = data
      console.log('this.users = ', this.users)
    }, () => console.log('err'))
  }

  ionViewDidLoad() {
    
  }
  
  openChannelModal(user) {
    let channelModal = this.modalCtrl.create(ChannelInfoPage, { user: user }, {cssClass: 'channel-modal'});
    channelModal.present();    
  }

}
