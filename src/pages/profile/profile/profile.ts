import {Component, OnInit} from '@angular/core';
import {NavController, NavParams, App, ModalController} from 'ionic-angular';
import {StartPage} from '../../authentication/start/start';
import {AuthServices} from '../../../providers/services/auth.service';
import {PayPage} from '../../pay/pay';
import {EditProfilePage} from '../edit-profile/edit-profile';
import {StreamService} from '../../../providers/services/stream.service';
import {ActiveStreamPage} from '../../stream/active-stream/active-stream';
import {NewStreamPage} from '../../stream/new-stream/new-stream';
import {IUserModel} from "../../../models/user";
import {CreateStreamPage} from "../../stream/create-stream/create-stream.page";
import {videojs} from 'video.js';
import {BroadcastingPage} from "../../stream/broadcasting/broadcasting.page";

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {
  user: IUserModel = null;
  streamChoice = 'upcomingStream';
  streams;
  endStreams;

  constructor(public navCtrl: NavController,
              public app: App,
              private authService: AuthServices,
              private modalCtrl: ModalController,
              private streamServ: StreamService) {
  }

  ngOnInit() {
    this.user = this.authService.getUser();
    this.streamServ.getSomeStreams({user: this.user.id, status: 1})
      .subscribe(streams => {
        this.streams = streams;
      });
    this.streamServ.getSomeStreams({user: this.user.id, status: 3})
      .subscribe(streams => {
        this.endStreams = streams;
      });
  }

  openTranslation(stream) {
    this.streamServ.setCurrentStream(stream);
    this.navCtrl.push(BroadcastingPage);
  }

  exitProfile() {
    this.authService.signOut();
    this.app.getRootNav().setRoot(StartPage);
  }

  showPayPage() {
    this.navCtrl.push(PayPage);
  }

  addStream() {
    let profileModal = this.modalCtrl.create(CreateStreamPage);
    profileModal.present();
    profileModal.onDidDismiss(param => {
      console.log('param = ', param);
    })
  }

  editProfile() {
    let profileModal = this.modalCtrl.create(
      EditProfilePage,
      {
        username: this.user.username,
        phone_number: this.user.phone_number,
        email: this.user.email,
        year: this.user.year
      });

    profileModal.present();
    profileModal.onDidDismiss(params =>
      this.user = params ? params.newUserInfo : this.user
    )
  }

  changePhoto() {
    console.log('here')
  }
}
