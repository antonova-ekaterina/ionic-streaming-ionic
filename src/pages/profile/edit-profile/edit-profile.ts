import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import {AuthServices} from '../../../providers/services/auth.service';

import emailMask from 'text-mask-addons/dist/emailMask';
import {ToastController, AlertController, NavParams, ViewController, App, NavController} from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {ProfilePage} from "../profile/profile";
import {TabsPage} from "../../tabs/tabs";
import {UserProvider} from "../../../providers/services/user.service";

@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html'
})
export class EditProfilePage implements OnInit {
  username: string = '';
  phone_number: string = '';
  email: string = '';
  year: string = null;
  newYear: string = '';
  validations_form: FormGroup;
  userId: string = null;

  emailMask = emailMask;
  base64Image;

  constructor(private formBuilder: FormBuilder,
              private toastCtrl: ToastController,
              private authServices: AuthServices,
              private userProvider: UserProvider,
              private alertCtrl: AlertController,
              private camera: Camera,
              private navParams: NavParams,
              private viewCtrl: ViewController) {
  }

  ngOnInit() {
    this.username = this.navParams.get('username');
    this.phone_number = this.navParams.get('phone_number');
    this.email = this.navParams.get('email');
    this.year = this.navParams.get('year').toString();
    this.newYear = this.year;
    this.userId = this.authServices.getUser().id;
    console.log('year = ', this.year);

    this.validations_form = this.formBuilder.group({
      name: new FormControl(''),
      phone_number: new FormControl(''),
      email: new FormControl('',
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      )
    });
  }

  validation_messages = {
    'name': [
      {type: 'required', message: 'Обязательное поле.'}
    ],
    'email': [
      {type: 'required', message: 'Обязательное поле.'},
      {type: 'pattern', message: 'введите действительный адрес электронной почты.'}
    ]
  };

  onEdit(values) {
    let newName = values.name;
    let newPhoneNumber = values.phone_number;
    let newEmail = values.email;
    let newYear = this.newYear;

    console.log('new name = ', newName);
    console.log('new phone number = ', newPhoneNumber);
    console.log('new Email = ', newEmail);
    console.log('new year = ', newYear);

    if (this.username !== newName || this.email !== newEmail || this.year !== newYear || this.phone_number !== newPhoneNumber) {
      console.log('here, year = ', newYear);
      this.userProvider.editProfile(this.userId, {
        "username": newName,
        "phone_number": newPhoneNumber,
        "email": newEmail,
        "year": newYear,
      })
        .subscribe((newUserInfo) => {
            this.authServices.setUser(newUserInfo);
            this.viewCtrl.dismiss({
              newUserInfo: newUserInfo
            });
          },
          err => console.log('err in onEdit user = ', err)
        )
    } else {
      this.dismiss()
    }
  }

  dismiss() {
    this.viewCtrl.dismiss(null);
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  addPhoto() {
    let alert = this.alertCtrl.create({
      title: 'Загрузить фото',
      buttons: [
        {
          text: 'Камера',
          handler: data => {
            let options: CameraOptions = {
              quality: 100,
              destinationType: this.camera.DestinationType.DATA_URL,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE
            }
            this.camera.getPicture(options).then((imageData) => {
              this.base64Image = 'data:image/jpeg;base64,' + imageData;
              this.userProvider.editProfile(this.userId, {"avatar": this.base64Image})
            }, (err) => {
              this.presentToast("Произошла ошибка");

            });
          }
        },
        {
          text: 'Галерея',
          handler: data => {
            let options: CameraOptions = {
              quality: 100,
              destinationType: this.camera.DestinationType.DATA_URL,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              sourceType: 0
            }

            this.camera.getPicture(options).then((imageData) => {
              this.base64Image = 'data:image/jpeg;base64,' + imageData;

              this.userProvider.editProfile(this.userId, {"avatar": this.base64Image})
            }, (err) => {
              this.presentToast("Произошла ошибка");

            });
          }
        }
      ]
    });
    alert.present();
  }
}
