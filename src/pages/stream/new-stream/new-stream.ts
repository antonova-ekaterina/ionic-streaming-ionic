import {Component, ViewChild, AfterViewInit, ElementRef} from '@angular/core';
import * as RecordRTC from 'recordrtc';
import MediaStreamRecorder from 'msr'
import {StreamService} from '../../../providers/services/stream.service';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ToastController, ModalController} from 'ionic-angular';
import {ActiveStreamPage} from '../active-stream/active-stream';

@Component({
  selector: 'page-new-stream',
  templateUrl: 'new-stream.html',
})

export class NewStreamPage implements AfterViewInit {
  @ViewChild('video') video: ElementRef;
  private stream: MediaStream;
  private recordRTC: any;

  private win: any = <any>window;
  private nav: any = <any>navigator;
  private isCapturing = false;
  showTestAudio = true;
  private audioContext;
  private processor;
  private tracks: Array<any> = [];
  videoMuted = true;
  sendStream: boolean = false;
  mediaRecorder: MediaStreamRecorder;
  audioRecorder: MediaStreamRecorder;
  sources
  dest

  onStream = false

  //stream form vars
  new_stream;
  startDate = false;
  price = false;
  today = new Date();
  todayDate = this.today.getUTCFullYear().toString() + '-' + this.getMonth() + '-' + this.getDay()
  public event = {
    month: this.todayDate,
    timeStarts: '07:43'
  }

  constructor(public streamServ: StreamService,
              public formBuilder: FormBuilder,
              private toastCtrl: ToastController,
              public modalCtrl: ModalController)  {
  }

  ionViewWillLoad() {
    this.new_stream = this.formBuilder.group({
      label: new FormControl('', Validators.required),
      description: new FormControl(''),
      price: new FormControl('')
    });
  }

  ngAfterViewInit() {
    // set the initial state of the video
    let video: HTMLVideoElement = this.video.nativeElement;
    video.muted = false;
    video.controls = true;
    video.autoplay = true;
    this.startCapture();
  }

  getMonth() {
    let month = this.today.getUTCMonth();
    if (month < 10)
      return '0' + month;
    else
      return month
  }

  getDay() {
    let day = this.today.getUTCDate();
    if (day < 10)
      return '0' + day;
    else
      return day
  }

  changeDate() {
    this.startDate = !this.startDate;
  }

  changePrice() {
    this.price = !this.price;
  }

  createStream(val) {
    let activeStream = this.modalCtrl.create(ActiveStreamPage, {
      streamId: '9',
      adminMode: true,
      streamName: 'test'
    });
    activeStream.present();
  }

  presentToast(message, time = 2000) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: time,
      position: 'top'
    });
    toast.present();
  }

  startCapture() {
    if (this.isCapturing) {
      return;
    }
    this.showTestAudio = false;

    this.audioContext = new this.win.AudioContext();
    const config = {
      bufferLen: 4096,
      numChannels: 2
    };
    if (this.audioContext.createJavaScriptNode) {
      this.processor =
        this.audioContext.createJavaScriptNode(
          config.bufferLen,
          config.numChannels,
          config.numChannels
        );
    } else if (this.audioContext.createScriptProcessor) {
      this.processor =
        this.audioContext.createScriptProcessor(
          config.bufferLen,
          config.numChannels,
          config.numChannels
        );
    }

    this.processor.connect(this.audioContext.destination);
    if (typeof this.nav.mediaDevices.getUserMedia === 'undefined') {
      this.nav.getUserMedia = (this.nav.getUserMedia ||
        this.nav.webkitGetUserMedia ||
        this.nav.mozGetUserMedia ||
        this.nav.msGetUserMedia);

      this.nav.getUserMedia({
        video: true,
        audio: false
      }, (stream) => {
        this.isCapturing = true;
        this.tracks = stream.getTracks();
        // this.microphone = this.audioContext.createMediaStreamSource(stream);
        // this.microphone.connect(this.processor);
        // this.analizer(this.audioContext);

        this.video.nativeElement.srcObject = stream;
        this.video.nativeElement.muted = this.videoMuted;
      }, (error) => {
        console.error(error);
      })
        .then((stream) => {
          this.isCapturing = true;
          this.tracks = stream.getTracks();
          // this.microphone = this.audioContext.createMediaStreamSource(stream);
          // this.microphone.connect(this.processor);
          // this.analizer(this.audioContext);

          this.video.nativeElement.srcObject = stream;
          this.video.nativeElement.muted = this.videoMuted;
        })
        .catch((error) => {
          console.error(error);
        });
    } else {
      this.nav.mediaDevices.getUserMedia = (
        this.nav.mediaDevices.getUserMedia ||
        this.nav.mediaDevices.webkitGetUserMedia ||
        this.nav.mediaDevices.mozGetUserMedia ||
        this.nav.mediaDevices.msGetUserMedia
      );

      this.nav.mediaDevices.getUserMedia({
        video: true,
        audio: false
      })
        .then((stream) => {
          this.isCapturing = true;
          this.tracks = stream.getTracks();
          // this.microphone = this.audioContext.createMediaStreamSource(stream);
          // this.microphone.connect(this.processor);
          // this.analizer(this.audioContext);

          console.log('VAR 3', stream);
          this.video.nativeElement.srcObject = stream;
          this.video.nativeElement.muted = this.videoMuted;
        })
        .catch((error) => {
          console.error(error);
        });
    }
    this.sendStream = true;
  }

  successCallback(stream: MediaStream) {

    var options = {
      mimeType: 'video/mp4\;codecs=vp8', // or video/webm\;codecs=h264 or video/webm\;codecs=vp9
      audioBitsPerSecorosknd: 128000,
      videoBitsPerSecond: 128000,
      bitsPerSecond: 128000 // if this line is provided, skip above two
    };
    this.stream = stream;
    this.recordRTC = RecordRTC(stream, options);
    this.recordRTC.startRecording();
    let video: HTMLVideoElement = this.video.nativeElement;
    video.src = window.URL.createObjectURL(stream);
  }

  errorCallback() {
  }

  processVideo(audioVideoWebMURL) {
    let video: HTMLVideoElement = this.video.nativeElement;
    let recordRTC = this.recordRTC;
    video.src = audioVideoWebMURL;
    var recordedBlob = recordRTC.getBlob();
    recordRTC.getDataURL(function (dataURL) {
    });
  }

  startRecording() {
    let mediaConstraints = {
      video: true,
      audio: true
    };
    navigator.getUserMedia(mediaConstraints, this.onMediaSuccess.bind(this), this.onMediaError);
    this.onStream = true;
  }

  onMediaSuccess(stream) {
    let _this = this
    this.mediaRecorder = new MediaStreamRecorder.MultiStreamRecorder(stream);
    this.mediaRecorder.mimeType = 'video/x-matroska;codecs=vp8';
    this.mediaRecorder.ondataavailable = (blobs) => {
      let videoURL = URL.createObjectURL(blobs.video);
      let audioURL = URL.createObjectURL(blobs.audio);
      let blob = new Blob([blobs.video, blobs.audio], {'type': 'video/mp4'})
      // _this.streamServ.getEndPoint(blobs.video)
      console.log(blobs, videoURL, audioURL)
    };
    this.mediaRecorder.start(3000);
  }

  onMediaError(e) {
    console.error('media error', e);
  }

  pauseStream() {
    this.mediaRecorder.pause();
  }

  stopStream() {
    this.mediaRecorder.stop()
  }

  download() {
    this.recordRTC.save('video.mkv');
  }

  dismiss() {
  }
}
