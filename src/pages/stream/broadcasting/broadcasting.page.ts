import {Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import io from "socket.io-client";
import {StreamService} from "../../../providers/services/stream.service";
import {IStream} from "../../../models/stream";
import * as PeerConnection from 'rtcpeerconnection';
import {SIGNALING_URL} from "../../../providers/api";

@Component({
  selector: 'app-broadcasting',
  templateUrl: 'broadcasting.page.html'
})
export class BroadcastingPage implements OnInit, OnDestroy {

  io: io.Socket = io(SIGNALING_URL);
  pc: PeerConnection;

  @ViewChild('videoContainer') videoContainer: ElementRef = null;
  screenHeight: string = '';
  currentStream: IStream = this.streamService.getCurrentStream();
  streamId: string;

  constructor(private navParams: NavParams,
              private navCtrl: NavController,
              private htmlRenderer: Renderer2,
              private streamService: StreamService) {
  }

  ngOnInit() {
    this.streamId = 'stream' + this.currentStream.id;
    this.io.on('connect', () => this.onSocketConnected());
    this.pc = new PeerConnection();

    this.screenHeight = window.innerHeight + 'px';

    StreamService.startTranslation(this.onSuccessGetMedia.bind(this), this.onErrorGetMedia.bind(this));
  }

  onSocketConnected() {
    this.io.emit('new-stream', {stream: this.currentStream, channel: this.streamId});

    this.pc.on('ice', (candidate) => {
      this.io.emit('candidate', candidate);
    });

    this.io.on('candidate', candidate => {
      this.pc.processIce(candidate);
    });

    this.io.on('answer', answer => {
      this.pc.handleAnswer(answer);
    });

    this.pc.offer({
      offerToReceiveAudio: true,
      offerToReceiveVideo: true
    }, (err, offer) => {
      if (err) {
        console.error('Create offer error', err);
        return alert('Cant create offer');
      }
      this.io.emit('offer', offer);
    });
  }

  onSuccessGetMedia(stream) {
    this.htmlRenderer.setProperty(this.videoContainer.nativeElement, 'srcObject', stream);
    this.pc.addStream(stream);
  }

  onErrorGetMedia(err) {
    console.log('err in get media = ', err);
    alert('Cant get user media');
    this.endTranslation();
  }

  endTranslation() {
    this.navCtrl.pop();
  }

  ngOnDestroy() {
    this.streamService.refreshStream(3);
  }
}
