import {AfterViewInit, Component, ElementRef, OnDestroy, Renderer2, ViewChild} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {StreamService} from "../../../providers/services/stream.service";
import * as PeerConnection from 'rtcpeerconnection';
import io from "socket.io-client";
import {SIGNALING_URL} from "../../../providers/api";

@Component({
  selector: 'show-stream',
  templateUrl: 'show-stream.page.html'
})
export class ShowStreamPage implements AfterViewInit, OnDestroy {

  io: io.Socket = io(SIGNALING_URL);
  pc: PeerConnection;

  @ViewChild('videoContainer') videoContainer: ElementRef = null;
  streamId: string = '';
  streamAdded: boolean = false;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private htmlRenderer: Renderer2,
              private streamService: StreamService) {
  }

  ngAfterViewInit() {

    this.streamId = 'stream' + this.navParams.get('id');
    this.io.on('connect', () => this.onSocketConnected());
    this.pc = new PeerConnection();

    this.pc.on('addStream', (stream) => {
      if (this.streamAdded) {
        return;
      }
      this.htmlRenderer.setProperty(this.videoContainer.nativeElement, 'srcObject', stream.stream);
      this.streamAdded = true;
    });
  }

  onSocketConnected() {
    this.io.emit('join', this.streamId);

    this.pc.on('ice', (candidate) => {
      this.io.emit('candidate', candidate);
    });

    this.io.on('candidate', candidate => {
      this.pc.processIce(candidate);
    });

    this.io.on('offer', (offer) => {
      this.pc.handleOffer(offer, (err) => {
        if (err) {
          console.error('Handle offer error', err);
          // return alert('Cant handle offer');
          return;
        }
        this.pc.answer((err, answer) => {
          if (err) {
            console.error('Create answer error', err);
            // return alert('Cant create answer');
            return;
          }
          this.io.emit('answer', answer);
        })
      })
    });
  }

  dismiss() {
    this.navCtrl.pop();
  }

  ngOnDestroy() {
  }
}
