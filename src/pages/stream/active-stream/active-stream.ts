import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';
import MediaStreamRecorder from 'msr'

import {WebsocketProvider} from '../../../providers/websocket';
import {ChatProvider} from '../../../providers/chat';
import {ApiService} from '../../../providers/api';
import {StreamService} from '../../../providers/services/stream.service';
import {AuthServices} from '../../../providers/services/auth.service';

const updateTime = 1000 * 5;

@Component({
  selector: 'page-active-stream',
  templateUrl: 'active-stream.html',
  providers: [
    WebsocketProvider,
    ChatProvider
  ]
})
export class ActiveStreamPage implements AfterViewInit, OnInit, OnDestroy {

  @ViewChild('video') video: ElementRef;
  private nav: any = <any>navigator;
  mediaRecorder: MediaStreamRecorder;

  comments = [];
  streamId;
  adminMode;
  streamName;
  notStream = true;
  intId;
  endpoint;

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private chatProvider: ChatProvider,
              private apiServices: ApiService,
              private streamServ: StreamService,
              private authServ: AuthServices) {
  }

  ngOnInit() {
    this.adminMode = this.navParams.get('adminMode');
    this.streamId = this.navParams.get('streamId');
    this.streamName = this.navParams.get('streamName');

    this.chatProvider.messages
      .subscribe(msg => {
        console.log("Response from websocket: " + msg);
        this.comments.push(msg)
      });
  }

  ngAfterViewInit() {
    this.initVideoState();
    if (this.adminMode) {
      this.startVideoCapture();
      this.startRecording();
    }
  }

  initVideoState() {
    let video: HTMLVideoElement = this.video.nativeElement;
    video.muted = false;
    video.controls = false;
    video.autoplay = true;
  }

  startVideoCapture() {
    const mediaDevice = this.nav.mediaDevices.getUserMedia;

    console.log('mediaDevice = ', mediaDevice);
    console.log('typeof mediaDevice = ', typeof mediaDevice);

    if (typeof mediaDevice === 'undefined') {
      this.nav.getUserMedia =
        this.nav.getUserMedia ||
        this.nav.webkitGetUserMedia ||
        this.nav.mozGetUserMedia ||
        this.nav.msGetUserMedia;

      console.log('getUserMedia = ', this.nav.getUserMedia);

      this.nav.getUserMedia({
          video: true,
          audio: false
        },
        stream => this.video.nativeElement.srcObject = stream,
        error => console.error(error)
      )
        .then(stream => this.video.nativeElement.srcObject = stream)
        .catch(error => console.error(error));

    } else {
      this.nav.mediaDevices.getUserMedia =
        this.nav.mediaDevices.getUserMedia ||
        this.nav.mediaDevices.webkitGetUserMedia ||
        this.nav.mediaDevices.mozGetUserMedia ||
        this.nav.mediaDevices.msGetUserMedia;

      console.log('getUserMedia = ', this.nav.mediaDevices.getUserMedia);

      this.nav.mediaDevices.getUserMedia({
        video: true,
        audio: true
      })
        .then(stream => {
          console.log('VAR 3', stream);
          this.video.nativeElement.srcObject = stream;
        })
        .catch(error => console.log('err = ', error));
    }
  }

  startRecording() {
    let mediaConstraints = {
      video: true,
      audio: true
    };
    navigator.getUserMedia(mediaConstraints, this.onMediaSuccess.bind(this), this.onMediaError);
  }

  onMediaSuccess(stream) {
    const parentThis = this;
    this.mediaRecorder = new MediaStreamRecorder(stream);
    this.mediaRecorder.ondataavailable = (blob) => {
      parentThis.streamServ.getEndPoint(blob)
    };
    this.mediaRecorder.start(updateTime);
  }

  onMediaError(e) {
    console.log('!media error', e);
  }

  stopStream() {
    this.mediaRecorder.stop();
    this.streamServ
      .refreshStream(3)
      .subscribe(
        data => console.log(data),
        err => console.log(err)
      )
  }

  addComment(val) {
    let token = this.authServ.getToken();
    let message = {
      'token': token,
      'message': {
        'text': val.value
      },
      'type': 'chat_message'
    };
    this.chatProvider.messages.next(message);
    val.value = '';
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  ngOnDestroy() {
    if (!this.adminMode) {
      clearInterval(this.intId);
    } else {
      this.stopStream()
    }
  }
}
