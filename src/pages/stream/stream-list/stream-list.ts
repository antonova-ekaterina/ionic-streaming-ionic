import {Component} from '@angular/core';
import {ModalController, NavController, PopoverController} from 'ionic-angular';
import {NewStreamPage} from '../new-stream/new-stream';
import {IStream} from '../../../models/stream';
import {ActiveStreamPage} from '../active-stream/active-stream';
import {ChannelInfoPage} from '../../channel-info/channel-info';
import {StreamService} from '../../../providers/services/stream.service';
import {FilterPage} from '../../filter/filter';
import {UserProvider} from "../../../providers/services/user.service";
import {AuthPage} from "../../authentication/auth/auth";
import {ShowStreamPage} from "../show-stream/show-stream.page";
import {CreateStreamPage} from "../create-stream/create-stream.page";

@Component({
  selector: 'page-stream-list',
  templateUrl: 'stream-list.html',
})
export class StreamListPage {

  streams = [];
  page: number = 1;

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              private streamServ: StreamService,
              private userProvider: UserProvider) {

  }

  ionViewWillLoad() {
    this.streamServ.getAllStreams({page: 1})
      .subscribe(data => {
        this.streams = data.streams
      })
  }

  doRefresh(refresher) {
    this.ionViewWillLoad();
    refresher.complete();
  }

  loadMore(infiniteScroll = null) {
    this.streamServ.getAllStreams({page: this.page + 1}).subscribe(data => {
      if (data) {
        if (!data || !data.results) {
          return;
        }
        this.page++;
        data.results.forEach(el => {
          this.streams.push(el)
        });
      }

      console.log(data);
      infiniteScroll.complete();
    })
  }

  filterShow() {
    let filterModal = this.modalCtrl.create(FilterPage, '', {cssClass: 'channel-modal'});
    filterModal.onDidDismiss(data => {
      if (data) {
        data.page = 1;
        this.streamServ.getAllStreams(data).subscribe(data => {
          console.log(data)
          this.streams = data.results
          console.log(this.streams)
        })
      } else {
        this.page = 1;
        this.streamServ.getAllStreams({page: 1}).subscribe(data => {
          console.log(data)
          this.streams = data.results
        })
      }

    });
    filterModal.present();
  }

  searchShow() {
  }

  addNewTranslyation() {
    let profileModal = this.modalCtrl.create(CreateStreamPage);
    profileModal.present();
    profileModal.onDidDismiss(param => {
      console.log('param = ', param);
    })
  }

  openStreamModal(id, name) {
    this.navCtrl.push(ShowStreamPage, {
      id: id
    });
  }

  openChannelModal(id) {
    this.userProvider.getSomeUser(id).subscribe(data => {
      let channelModal = this.modalCtrl.create(ChannelInfoPage, {user: data.user}, {cssClass: 'channel-modal'});
      channelModal.present();
    }, error => console.log(error))
  }

  addToSub(userId) {
    console.log("add to subscribes, user id", userId);
  }
}
