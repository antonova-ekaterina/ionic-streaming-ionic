import {Component} from "@angular/core";
import {StreamService} from "../../../providers/services/stream.service";
import {NavController, ViewController} from "ionic-angular";
import {BroadcastingPage} from "../broadcasting/broadcasting.page";
import {CommonService} from "../../../providers/services/common.service";

@Component({
  selector: 'create-stream',
  templateUrl: 'create-stream.page.html'
})
export class CreateStreamPage {
  stream = {
    name: '',
    description: '',
    start_now: true,
    date_start: '2019-01-01',
    time_start: '00:00',
    duration: '00:00',
    free: true,
    freetime: '00:00',
    price: '0',
    chat_mode: true
  };

  constructor(private streamService: StreamService,
              private viewCtrl: ViewController,
              private navCtrl: NavController) {
  }

  submit() {
    const newStream = this.setStreamParameters();
    this.createStream(newStream);
  }

  dismiss() {
    this.viewCtrl.dismiss(null);
  }

  createStream(newStream) {
    this.streamService.createStream(newStream)
      .subscribe(stream => {
        this.streamService.setCurrentStream(stream);
        this.viewCtrl.dismiss({newStream: stream});
        if (this.stream.start_now) {
          this.navCtrl.push(BroadcastingPage)
        }
      })
  }

  setStreamParameters() {
    const newStream = {
      price: this.stream.free ? 0 : this.stream.price,
      chat_mode: this.stream.chat_mode,
      free: this.stream.free,
      time_start: this.stream.start_now ?
        new Date() :
        CommonService.calculateDate(
          this.stream.date_start,
          this.stream.time_start
        ),
      name: this.stream.name,
      freetime: this.stream.free ? 0 : CommonService.calculateTimeInMinute(this.stream.freetime),
      description: this.stream.description,
      duration: CommonService.calculateTimeInMinute(this.stream.duration),
      status: '1',
      record_url: "",
    };
    return newStream;
  }
}
