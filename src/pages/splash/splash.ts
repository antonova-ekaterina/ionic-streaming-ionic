import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {AuthServices} from '../../providers/services/auth.service';
import {StartPage} from '../authentication/start/start';
import {TabsPage} from '../tabs/tabs';

@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage {

  constructor(public navCtrl: NavController,
              private authService: AuthServices) {

    this.authService.isAuthorized()
      .then((isAuthorized: boolean) => {
        let page = isAuthorized ? TabsPage : StartPage;
        this.navCtrl.setRoot(page);
      })
  }


}
