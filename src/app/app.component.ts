import {Component} from '@angular/core';
import {Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {SplashPage} from '../pages/splash/splash';

import {AndroidPermissions} from '@ionic-native/android-permissions';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = SplashPage;

  constructor(private androidPermissions: AndroidPermissions,
              platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen) {
    platform
      .ready()
      // .then(() => this.getPermission())
      .then(() => {
        statusBar.styleDefault();
        splashScreen.hide();
      });
  }

  getPermission() {
    return this.androidPermissions.requestPermissions([
      this.androidPermissions.PERMISSION.CAMERA,
      this.androidPermissions.PERMISSION.ALLMEDIA,
      this.androidPermissions.PERMISSION.MICROPHONE,
      this.androidPermissions.PERMISSION.RECORD_AUDIO,
      this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS,
      this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
    ]);
  }
}
