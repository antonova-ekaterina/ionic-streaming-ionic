import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from "@ionic/storage";
import { MyApp } from './app.component';
import { GooglePlus } from '@ionic-native/google-plus';
import { Camera } from '@ionic-native/camera';
import { CameraPreview } from '@ionic-native/camera-preview';

import { AuthPage } from '../pages/authentication/auth/auth';
import { RegistrationPage } from '../pages/authentication/registration/registration';
import { ProfilePage } from '../pages/profile/profile/profile';
import { StreamListPage } from '../pages/stream/stream-list/stream-list';
import { TabsPage } from '../pages/tabs/tabs'; 
import { SplashPage } from '../pages/splash/splash';
import { NewStreamPage } from '../pages/stream/new-stream/new-stream';
import { ActiveStreamPage } from '../pages/stream/active-stream/active-stream';
import { StartPage } from '../pages/authentication/start/start';
import { ChannelInfoPage } from '../pages/channel-info/channel-info';
import { SubscribesPage } from '../pages/subscribes/subscribes';
import { PayPage } from '../pages/pay/pay';
import { UsersListPage } from '../pages/users-list/users-list';
import { EditProfilePage } from '../pages/profile/edit-profile/edit-profile';
import { FilterPage } from '../pages/filter/filter';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TextMaskModule } from 'angular2-text-mask';
import { AuthServices } from '../providers/services/auth.service';
import {ApiService} from '../providers/api';
import { StreamService } from '../providers/services/stream.service';
import { WebsocketProvider } from '../providers/websocket';
import { ChatProvider } from '../providers/chat';
import { UserProvider } from '../providers/services/user.service';
import {CreateStreamPage} from "../pages/stream/create-stream/create-stream.page";
import {ShowStreamPage} from "../pages/stream/show-stream/show-stream.page";
import {BroadcastingPage} from "../pages/stream/broadcasting/broadcasting.page";
import {AndroidPermissions} from "@ionic-native/android-permissions";
import {CommonService} from "../providers/services/common.service";
import {broadcastService} from "../providers/services/broadcast.service";

@NgModule({
  declarations: [
    MyApp,
    SplashPage,
    TabsPage,
    StartPage,
    AuthPage,
    RegistrationPage,
    ProfilePage,
    StreamListPage,
    NewStreamPage,
    ActiveStreamPage,
    ChannelInfoPage,
    SubscribesPage,
    PayPage,
    UsersListPage,
    EditProfilePage,
    FilterPage,
    CreateStreamPage,
    ShowStreamPage,
    BroadcastingPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      monthNames: ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'],
      monthShortNames: ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сент', 'окт', 'нояб', 'дек'],
      dayNames: ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'],
      dayShortNames: ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'],
    }),
    IonicStorageModule.forRoot({
      name: 'ria-db',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    TextMaskModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SplashPage,
    TabsPage,
    StartPage,
    AuthPage,
    RegistrationPage,
    ProfilePage,
    StreamListPage,
    NewStreamPage,
    ActiveStreamPage,
    ChannelInfoPage,
    SubscribesPage,
    PayPage,
    UsersListPage,
    EditProfilePage,
    FilterPage,
    CreateStreamPage,
    ShowStreamPage,
    BroadcastingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    CameraPreview,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServices,
    ApiService,
    CommonService,
    GooglePlus,
    StreamService,
    WebsocketProvider,
    ChatProvider,
    UserProvider,
    AndroidPermissions,
    broadcastService
  ],
  exports: [TextMaskModule]
})
export class AppModule {}
