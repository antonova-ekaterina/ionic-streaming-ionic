import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpHeaders} from '@angular/common/http';
import {ServerResponse} from '../models/server-response';
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';

export const BASE_URL: string = 'http://wilix.org:8001/api/v1/';
export const SIGNALING_URL: string = 'http://10.1.1.15:8080/';

@Injectable()
export class ApiService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  get(path: string, params = null): Observable<any> {
    let resultPath = `${BASE_URL}${path}`;
    if (params) {
      resultPath += '?';
      Object.keys(params).forEach(key => {
        resultPath += `${key}=${params[key]}&`;
      });
      resultPath = resultPath.slice(0, -1);
    }

    return this.http
      .get(resultPath, this.httpOptions)
      .map((data: ServerResponse) => {
        if (!data.success) {
          console.error('Server response: ', data.errors);
          throw new Error(data.errors.toString());
        }

        return data.data;
      });
  }

  post(path, body = null) {
    let resultPath = `${BASE_URL}${path}/`;
    return this.http
      .post(resultPath, body, this.httpOptions)
      .map((data: ServerResponse) => {
        if (!data.success) {
          console.error('Server response: ', data.errors);
          throw data.errors;
        }
        return data.data;
      });
  }

  put(path, params) {
    let resultPath = `${BASE_URL}${path}`;
    return this.http
      .put(resultPath, params, this.httpOptions)
      .map((data: ServerResponse) => {
        if (!data.success) {
          console.error('Server response: ', data.errors);
          throw data.errors;
        }
        return data.data;
      });
  }

  delete(path, params) {
    let resultPath = `${BASE_URL}${path}`;
    if (params) {
      resultPath += '?';
      Object.keys(params).forEach(key => {
        resultPath += `${key}=${params[key]}&`;
      });
      resultPath = resultPath.slice(0, -1);
    }

    return this.http
      .delete(resultPath, this.httpOptions)
      .map((data: ServerResponse) => {
        if (!data.success) {
          console.error('Server response: ', data.errors);
          throw data.errors;
        }

        return data.data;
      });
  }

  setToken(Authorization) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'accept': 'application/json',
        'X-CSRFToken': 'e8fhrJbLjh93AElVVjD8rVYkoOXxWf3vfITtJ2rgw4e9lSOVxCUGnSWYAodVUAD5',
        'Authorization': Authorization
      })
    };
  }

  clearHeaders() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'accept': 'application/json',
        'X-CSRFToken': 'GuFZR1Ena1Ngoya2dsDrXDt6TnfTKgAdH4jb9kUSnOSm9MD2PLUZTArK5XvhIBaN'
      })
    }
  }

  postStream(path, body, headers) {
    return this.http.post(path, body, {headers: new HttpHeaders(headers)})
      .map((data: any) => {
        return data;
      });
  }

  getStream(path, body, headers) {
    return this.http
      .post(path, body, {headers: new HttpHeaders(headers)})
      .map((data: any) => {
        return data;
      });
  }

  postEndPoint(path, body, headers) {
    return this.http
      .post(path, body, {headers: new HttpHeaders(headers)})
      .map((data: any) => {
        return data.DataEndpoint;
      });
  }

}
