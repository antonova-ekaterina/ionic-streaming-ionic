import {Injectable} from '@angular/core';
import {ApiService} from '../api';

import {IStream} from "../../models/stream";
import {CommonService} from "./common.service";

@Injectable()
export class StreamService {

  currentStream: IStream = null;

  constructor(public apiService: ApiService) {
  }

  getStreamLinkById(id: string) {
    let testLink = 'https://0176f6.entrypoint.cloud.wowza.com/app-627c/ngrp:2cb3cc1a_all/playlist.m3u8';
    return this.apiService.get(`streams/${id}`)
      .map(res => testLink)
      .catch((e: any) => CommonService.onGetError(e,'err in getting stream link = '));
  }

  createStream(stream) {
    return this.apiService.post('streams', stream)
      .map(res => res.stream)
      .catch((e: any) => CommonService.onGetError(e,'err in creating stream = '));
  }

  getAllStreams(params = null) {
    return this.apiService.get('streams/', params)
      .map(data => data)
      .catch((e: any) => CommonService.onGetError(e,'err in getting all streams = '));
  }

  getSomeStreams(params = null) {
    return this.apiService.get('streams/', params)
      .map((data: any) => (!data || !data.streams) ? [] : data.streams)
      .catch((e: any) => CommonService.onGetError(e,'err in getting streams = '));
  }

  refreshStream(status): Promise<any> {
    const streamId = this.currentStream.id;
    return this.apiService
      .put(`streams/${streamId}/`, {status: status})
      .catch((e: any) => CommonService.onGetError(e,'err in refresh stream = '))
      .toPromise()
  }

  getEndPoint(body) {
    // const streamId = this.currentStream.id;
    const streamId = '25';
    const path = `http://wilix.org:8001/api/v1/streams/${streamId}/upload/`;

    this.apiService.postStream(path, body, {'content-type': 'video/mp4'})
      .catch((e: any) => CommonService.onGetError(e,'err in streaming = '))
      .subscribe()
  }

  setCurrentStream(stream: IStream) {
    return this.currentStream = stream;
  }

  getCurrentStream(): IStream {
    return this.currentStream;
  }

  static getUserMediaFunction() {
    let n = <any>navigator;

    if (n.mediaDevices === undefined) {
      console.log('have note mediaDevices');
      return null;
    }
    if (n.mediaDevices.getUserMedia === undefined) {
      console.log('You are using a browser that does not support the Media Capture API');
      return null;
    }
    return n.mediaDevices.getUserMedia;
  }

  static startTranslation(successFunc, errFunc) {
    const getUserMediaFunc = StreamService.getUserMediaFunction();

    getUserMediaFunc({
      video: {facingMode: "user"},
      // video: true,
      audio: true
    })
      .then(successFunc)
      .catch(errFunc)
  }
}
