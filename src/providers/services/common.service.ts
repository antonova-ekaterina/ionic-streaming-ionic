import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

@Injectable()
export class CommonService {

  static calculateDate(dateStr, timeStr) {
    const year = parseInt(dateStr.substr(0, 4));
    const month = parseInt(dateStr.substr(5, 2));
    const day = parseInt(dateStr.substr(8, 2));
    const hour = parseInt(timeStr.substr(0, 2));
    const minutes = parseInt(timeStr.substr(3, 2));
    return new Date(year, month, day, hour, minutes);
  }

  static calculateTimeInMinute(string) {
    const hour = parseInt(string.substr(0, 2));
    const minutes = parseInt(string.substr(3, 2));
    return hour * 60 + minutes;
  }

  static onGetError(err, message: string = 'err = '){
    return Observable.throw(console.log(message, err))
  }
}
