import {Injectable} from '@angular/core';
import {Storage} from "@ionic/storage";
import {Observable} from 'rxjs/Observable';
import {ReplaySubject} from "rxjs/ReplaySubject";
import {ApiService} from '../api';
import {IUserModel} from '../../models/user';

@Injectable()
export class AuthServices {

  public users: ReplaySubject<IUserModel> = new ReplaySubject(1);
  private user: IUserModel = null;
  private token: string = null;

  constructor(private api: ApiService,
              private storage: Storage) {
  }

  getToken() {
    return this.token;
  }

  setToken(token: string) {
    this.token = token;
    this.api.setToken('Token ' + this.token);
  }

  getUser() {
    return this.user;
  }

  setUser(user: IUserModel) {
    this.user = user;
  }

  refreshUser(userId: string) {
    return this.api.get(`accounts/${userId}/`)
      .toPromise()
  }

  isAuthorized() {
    return Promise.all([
      this.storage.get('token'),
      this.storage.get('user')
    ])
      .then(([token, user]) => {
        if (!(token && user)) {
          throw new Error();
        }
        this.setToken(token);
        return this.refreshUser(user.id)
      })
      .then(res => {
        this.setUser(res.user);
        return true;
      })
      .catch(err => {
        console.log('user not authorized');
        this.storage.clear();
        return false;
      })
  }

  signOut() {
    this.storage.set('token', null);
    this.storage.set('user', null);
    this.setUser(null);
    this.setToken(null)
  }

  verification(code, phone): Observable<any> {
    console.log("GET VARIFICATION")
    return this.api
      .post('accounts/phone_verification', {"phone_number": phone, "code": code})
      .map(({token, user}) => {
        this.user = user;
        this.storage.set('user', user);
        this.token = token;
        this.storage.set('token', token);
        this.api.setToken('Token ' + token);
        return user;

      })

  }

  signin(phone): Observable<any> {
    this.storage.remove('token');
    this.api.clearHeaders();
    return this.api
      .post('accounts/login', {"phone_number": phone})
      .map((data) => {
        console.log(data);
        return data.code;
      })
  }

  signup(username, email, phone): Observable<any> {
    return this.api
      .post('accounts', {"email": email, 'username': username, 'phone_number': phone})
      .flatMap((data) => {
        console.log(data)
        return this.signin(phone);
      })
  }

  signupGoogle(token) {
    return this.api
      .post('social/google', {'access_token': token})
      .map((data) => {
        console.log(data)
        // this.user = user;
        // this.token = token;
        // this.storage.set('token', token);
        // this.storage.set('user', user);
        // token = 'Token ' + token;
        // this.api.setToken(token);
        return data;
      })
  }

  // change service


  getAllUser(): Observable<any> {
    this.api.get('accounts/')
      .subscribe(
        (data: any) => {
          this.users.next(data.users)
          console.log(data)
        },
        () => {
          console.log('test')
        }
      )
    return this.users.asObservable();
  }


}
