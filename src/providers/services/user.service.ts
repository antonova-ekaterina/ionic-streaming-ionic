import {Injectable} from '@angular/core';
import {ApiService} from '../api';
import {Observable} from 'rxjs/Observable';
import {ReplaySubject} from 'rxjs';
import {IUserModel} from "../../models/user";

@Injectable()
export class UserProvider {
  channel = new ReplaySubject(1);

  constructor(private api: ApiService) {
  }

  editProfile(id: string, data) {
    return this.api.put(`accounts/${id}/`, data)
      .map(res => res.user)
  }

  getAllFavoriteChanner(data) {
    this.api.get('subscriptions/', data)
      .subscribe(
        (data: any) => {
          if (data) {
            this.channel.next(data.subscriptions)
          } else {
            this.channel.next(null)
          }
          console.log(data)
        },
        (err) => {
          console.log('err', err)
        }
      )
    return this.channel.asObservable();
  }

  addFavoriteChannel(userId, favoriteId): Observable<any> {
    return this.api
      .post('subscriptions', {"subscriber": userId, "author": favoriteId})
      .map((data) => {
        return data
      });
  }

  removeFavoriteChannel(id, userId, favoriteId): Observable<any> {
    return this.api
      .delete(`subscriptions/${id}/`, {"subscriber": userId, "author": favoriteId})
      .map((data) => {
        return data
      });
  }

  getSomeUser(id): Observable<any> {
    let userInfo = new ReplaySubject(1);
    this
      .api
      .get(`accounts/${id}/`)
      .subscribe(
        (user: IUserModel) => {
          userInfo.next(user)
        },
        () => {
          console.log('error')
        }
      );
    console.log(userInfo)
    return userInfo.asObservable();
  }
}
