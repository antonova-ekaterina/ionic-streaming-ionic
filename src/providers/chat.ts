import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { WebsocketProvider } from './websocket';
import { NavParams } from 'ionic-angular';


const CHAT_URL = 'ws://wilix.org:8001/ws/chat/';

// export interface Message {
//   success: boolean,
//   data: {
//     message: {
//       id: number,
//       created: string,
//       user: string,
//       text: string,
//       pin: boolean
//     }
//   }
// }

@Injectable()
export class ChatProvider {
  message;
  streamId;

  public messages: Subject<any>;

  constructor(private wsProvider: WebsocketProvider,public navParams: NavParams) {

    this.streamId = navParams.get('streamId');
    this.messages = <Subject<any>>wsProvider
      .connect(`${CHAT_URL}${this.streamId}/`)
      .map((response: MessageEvent): any => {
        let data = JSON.parse(response.data);
        console.log(data)
        if (!data.error) {
          return {
            success: data.success,
            data: {
              message: {
                id: data.data.message.id,
                created: data.data.message.created,
                user: data.data.message.user,
                text: data.data.message.text,
                pin: data.data.message.pin
              }
            }
          }
        }
        else {
          console.log('error', data)
        }
      });
  }
}
